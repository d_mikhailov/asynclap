﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynclap.Domains.Identity;

namespace Asynclap.Repositories.EntityFramework.Helpers
{
    public static class RoleFactory
    {
        public static Role CreateRole(string rolename)
        {
            return new Role() {
                Name = rolename
            };
        }
    }
}
