﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynclap.Domains.Enums;
using Asynclap.Domains.Identity;
using Asynclap.Repositories.EntityFramework.Helpers;

namespace Asynclap.Repositories.EntityFramework
{
    public class Initializer : DropCreateDatabaseIfModelChanges<Context>
    {
        protected override void Seed(Context context)
        {
            var roles = new List<Role>() {
                RoleFactory.CreateRole(Roles.admin.ToString()),
                RoleFactory.CreateRole(Roles.user.ToString())
            };

            context.Roles.AddRange(roles);
            context.SaveChanges();
        }
    }
}
