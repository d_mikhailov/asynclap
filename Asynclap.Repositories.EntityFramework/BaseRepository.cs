﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynclap.Repositories.Interfaces;


namespace Asynclap.Repositories.EntityFramework
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected DbContext _context;

        public BaseRepository(DbContext context)
        {
            _context = context;
        }

        public IQueryable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public async Task<T> GetAsync(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public void Create(T dataObject)
        {
            _context.Set<T>().Add(dataObject);
        }

        public void Delete(T dataObject)
        {
            _context.Set<T>().Remove(dataObject);
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
