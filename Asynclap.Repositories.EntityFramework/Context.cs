﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynclap.Domains.Identity;

namespace Asynclap.Repositories.EntityFramework
{
    public class Context : DbContext
    {
        public Context() : base("SQLAsynclap") { }
        public Context(string nameOrConnectionString) : base(nameOrConnectionString) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Account-Role many-to-many relationship
            modelBuilder.Entity<Account>().HasMany(c => c.Roles)
                .WithMany(s => s.Accounts)
                .Map(t => t.MapLeftKey("AccountId")
                            .MapRightKey("RoleId")
                            .ToTable("AccountRole"));
            #endregion
            /*
            modelBuilder
            .Entity<Account>()
            .ToTable("Accounts");

            modelBuilder.Entity<SocialNetworkAccount>()
                .Map<AsynclapAccount>(t => t.Requires("Type").HasValue("AsynclapAccount").IsRequired());*/
        }

        public DbSet<Account> Accounts { get; set; } 
        public DbSet<SocialNetworkAccount> SocialNetworkAccounts { get; set; }
        public DbSet<AsynclapAccount> AsynclapAccounts { get; set; }

        public DbSet<Role> Roles { get; set; }
    }
}
