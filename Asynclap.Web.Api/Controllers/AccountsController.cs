﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Asynclap.Domains.Enums;
using Asynclap.Domains.Identity;
using Asynclap.Repositories.Interfaces;
using Asynclap.Web.Api.Models;


namespace Asynclap.Web.Api.Controllers
{
    [RoutePrefix("api/accounts")]
    public class AccountsController : ApiController
    {
        private readonly IBaseRepository<Account> _accountsRepository;
        private readonly IBaseRepository<Role> _rolesRepository;

        public AccountsController(IBaseRepository<Account> accountsRepository, IBaseRepository<Role> rolesRepository)
        {
            _accountsRepository = accountsRepository;
            _rolesRepository = rolesRepository;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("GetRoles")]
        public async Task<IHttpActionResult> GetRoles()
        {
            var roles = await _rolesRepository.GetAll().ToListAsync();
            return Ok(roles);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("CreateAsynclapAccount")]
        public async Task<IHttpActionResult> CreateAsynclapAccount([FromBody]SignupViewModel model)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var account = new Account() { Location = model.Location };
            account.AddSocialNetworkAccount(new AsynclapAccount() { Uid = model.Email, Password = model.Password, Name = model.Name });
            _accountsRepository.Create(account);
            await _accountsRepository.SaveAsync();

            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("SocialNetworkAccount")]
        public async Task<IHttpActionResult> SocialNetworkAccount([FromBody] SocialSigninViewModel model)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var account = await _accountsRepository.GetAll().Include(s => s.SocialNetworkAccounts).Include(r => r.Roles).SingleOrDefaultAsync(a => a.SocialNetworkAccounts.Any(sa => sa.Uid == model.Email));
            if (account == null) {
                account = new Account() { Location = string.Format("London") };
                account.AddSocialNetworkAccount(new SocialNetworkAccount() { Uid = "facebookuid", Name = "facebookname", SocialNetworkProviderType = SocialNetworkProviderType.Facebook });
                _accountsRepository.Create(account);
                await _accountsRepository.SaveAsync();
                return Ok();
            }

            account.AddSocialNetworkAccount(new SocialNetworkAccount() { Uid = "facebookuid", Name = "facebookname", SocialNetworkProviderType = SocialNetworkProviderType.Facebook });
            await _accountsRepository.SaveAsync();
            return Ok();
        }

        [HttpGet]
        [Route("GetAccount")]
        public async Task<IHttpActionResult> GetAccountById(int id)
        {
            var account = await _accountsRepository.GetAsync(id);
            return Ok(account);
        }
    }
}
