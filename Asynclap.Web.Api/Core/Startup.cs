﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Asynclap.Web.Api.App_Start;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Asynclap.Web.Api.Core.Startup))]

namespace Asynclap.Web.Api.Core
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            DIConfig.RegisterDI(GlobalConfiguration.Configuration);
            DatabaseConfig.RegisterDatabase();
        }
    }
}
