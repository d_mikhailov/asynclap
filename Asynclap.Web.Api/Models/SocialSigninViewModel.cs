﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Asynclap.Domains.Enums;


namespace Asynclap.Web.Api.Models
{
    public class SocialSigninViewModel
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public SocialNetworkProviderType SocialNetworkProviderType { get; set; }
    }
}