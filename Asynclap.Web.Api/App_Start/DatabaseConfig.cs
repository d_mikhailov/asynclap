﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Asynclap.Repositories.EntityFramework;


namespace Asynclap.Web.Api.App_Start
{
    public static class DatabaseConfig
    {
        public static void RegisterDatabase()
        {
            Database.SetInitializer(new Initializer());
        }
    }
}