﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Asynclap.Domains.Identity;
using Asynclap.Repositories.EntityFramework;
using Asynclap.Repositories.Interfaces;
using Autofac;
using Autofac.Integration.WebApi;


namespace Asynclap.Web.Api
{
    public class DIConfig
    {
        public static void RegisterDI(HttpConfiguration configuration)
        {
            // Autofac, create container
            var builder = new ContainerBuilder();

            // Register WebApi Controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Database settings.
            builder.RegisterType<Context>()
                .As<DbContext>()
                .WithParameter("nameOrConnectionString", "SQLAsynclap")
                .InstancePerRequest();

            // Add repositories.
            builder.RegisterType<BaseRepository<Account>>()
                .As<IBaseRepository<Account>>();
            builder.RegisterType<BaseRepository<Role>>()
                .As<IBaseRepository<Role>>();

            // Build the container
            var container = builder.Build();

            // Create dependency resolver
            var resolver = new AutofacWebApiDependencyResolver(container);

            // Configure Web API with dependency resolver
            configuration.DependencyResolver = resolver;
        }
    }
}