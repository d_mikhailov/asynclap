﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynclap.Domains.Enums;


namespace Asynclap.Domains.Identity
{
    public class Account
    {
        public int AccountId { get; set; }
        public DateTimeOffset CreationDate { get; set; }
        public AccountStatuses AccountStatus { get; set; }
        public string Location { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<SocialNetworkAccount> SocialNetworkAccounts { get; set; }

        public Account()
        {
            CreationDate = DateTimeOffset.Now;
            AccountStatus = AccountStatuses.Active;
            Roles = new List<Role>();
            SocialNetworkAccounts = new List<SocialNetworkAccount>();
        }

        public void AddSocialNetworkAccount(SocialNetworkAccount socialNetworkAccount)
        {
            var numberOfAccountsWithEqualId = SocialNetworkAccounts.Count(s => s.SocialNetworkProviderType == socialNetworkAccount.SocialNetworkProviderType);
            if (numberOfAccountsWithEqualId != 0) {
                throw new Exception(String.Format("User already has social account with id '{0}'", socialNetworkAccount.SocialNetworkProviderType.ToString()));
            }

            SocialNetworkAccounts.Add(socialNetworkAccount);
        }
    }
}
