﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynclap.Domains.Enums;


namespace Asynclap.Domains.Identity
{
    public class AsynclapAccount : SocialNetworkAccount
    {
        public AsynclapAccount()
        {
            SocialNetworkProviderType = SocialNetworkProviderType.Asynclap;
        }
        public string Password { get; set; }
    }
}
