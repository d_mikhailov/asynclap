﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Asynclap.Domains.Enums;


namespace Asynclap.Domains.Identity
{
    public class SocialNetworkAccount
    {
        public int SocialNetworkAccountId { get; set; }
        public string Uid { get; set; }
        public string Name { get; set; }
        public SocialNetworkProviderType SocialNetworkProviderType { get; set; }
    }
}
