﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asynclap.Domains.Identity
{
    public class Role
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Account> Accounts { get; set; }

        public Role()
        {
            Accounts = new List<Account>();
        }
    }
}
