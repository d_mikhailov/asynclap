﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asynclap.Domains.Enums
{
    public enum AccountStatuses
    {
        NotActivated = 0,
        Active = 1,
        Blocked = 2
    }
}
