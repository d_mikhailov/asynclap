﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asynclap.Domains.Enums
{
    public enum SocialNetworkProviderType
    {
        Asynclap = 0,
        Facebook = 1,
        Google = 2,
        Twitter = 3
    }
}
